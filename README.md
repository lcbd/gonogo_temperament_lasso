# Go/No-Go Temperament LASSO
## About
These repository contains the code for the analysis described in:

Fishburn, F.A., Hlutkowsky, C.O., Bemis, L.M., Huppert, T.J., Wakschlag, L.S., & Perlman, S.B. (2019). Irritability uniquely predicts prefrontal cortex activation during preschool inhibitory control among all temperament domains: A LASSO approach. _NeuroImage_, 184, 68-77. doi: [10.1016/j.neuroimage.2018.09.023](https://doi.org/10.1016/j.neuroimage.2018.09.023)
