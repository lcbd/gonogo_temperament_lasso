function results = run_analysis(raw, CBQ, cbqnames)
% This function performs the analysis described in: 
%
% Fishburn F.A., Hlutkowsky C.O., Bemis L.M, Huppert T.J., Wakschlag L.S., 
%   & Perlman S.B. (2019). Irritability uniquely predicts prefrontal cortex 
%   activation during preschool inhibitory control among all temperament domains:
%   A LASSO approach. NeuroImage, 184, 68-77. doi: 10.1016/j.neuroimage.2018.09.023
%
% Usage: results = run_analysis(raw, CBQ, cbqnames);
%
% Inputs
%   raw: [# of subjects x 1] array of nirs.core.Data objects of raw intensity data
%   CBQ: [# of subjects x 15] matrix of CBQ values
%   cbqnames: [1 x 15] cell array of the names of each CBQ dimension
%
% Outputs
%   results: a struct containing the 1st- and 2nd-level activation results and LASSO model
%
% Note that the analysis in the paper was run using the 2018-05-31 version of 
% NIRS Brain AnalyzIR toolbox, but some necessary modules were not in the 
% official toolbox at that time.
%
% In addition, certain functions used here are from my personal repo:
%   univariate_outliers
%   multivariate_outliers
%   whiten_data
%

%% Preprocessing
job = nirs.modules.TrimBaseline();
    job.preBaseline = 5;
    job.postBaseline = 5;
job = nirs.modules.OpticalDensity(job);
job = nirs.modules.TDDR(job);
job = nirs.modules.BeerLambertLaw(job);
job = nirs.modules.KeepTypes(job);
    job.types = 'hbo';

hb = job.run(raw);

%% 1st-level activation
canon = nirs.design.basis.Canonical();
canon.incDeriv = true; % include temporal + dispersion derivatives in model

job = nirs.modules.AR_IRLS();
job.basis('default') = canon;
job.trend_func = @(t) nirs.design.trend.legendre(t,3); % detrending polynomial

SubjStats = job.run(hb);

% Keep only the canonical response
SubjStats = SubjStats.ttest({'NoGo:01','Motor:01'},[],{'NoGo','Motor'});

%% 2nd-level activation
job = nirs.modules.MixedEffects();
job.formula = 'beta ~ -1 + cond + (1|Name)';
job.robust = true;

GroupModel = job.run(SubjStats);

GroupStats  = GroupModel.ttest({'NoGo','Motor','NoGo-Motor'});

%% Compute the subject-level average of channels significant at group level
% Create ROI table with group-level significance mask
ROItbl = nirs.util.threshold2ROI( GroupModel.ttest('NoGo-Motor') , 'q<.05' , 'hbo' );

% Average across channels in ROI table for each subject
job = nirs.modules.ApplyROIMFX();
job.listOfROIs = ROItbl;

SubjAvg = job.run(SubjStats);

%% Extract averaged values
tstats = zeros(length(SubjAvg),1);
for i = 1:length(SubjAvg)
    tstats(i) = SubjAvg(i).ttest('NoGo-Motor').tstat;
end

%% Remove univariate outliers
[isunioutlier, univar_score] = univariate_outliers([tstats CBQ],1,3);
tstats(isunioutlier) = [];
CBQ(isunioutlier,:) = [];

%% Remove multivariate outliers
[ismultioutlier, multivar_dist] = multivariate_outliers([tstats CBQ],1,.0001);
tstats(ismultioutlier) = [];
CBQ(ismultioutlier,:) = [];

%% Decorrelate CBQ data via ZCA-cor
CBQ = whiten_data(CBQ,'ZCA-cor');

%% Perform lasso
opts = statset;
opts.UseParallel = true;

cv = cvpartition(length(tstats),'HoldOut',0.1);

[B,FitInfo] = lassoglm( CBQ, tstats, 'normal', ...
    'Alpha',1 ,'PredictorNames',cbqnames,...
    'CV', cv, 'MCReps', 10000, 'Options', opts );

%% Export results
results = [];
results.SubjStats = SubjStats;
results.GroupStats = GroupStats;
results.SubjAvg = SubjAvg;
results.isunioutlier = isunioutlier;
results.univar_score = univar_score;
results.ismultioutlier = ismultioutlier;
results.multivar_dist = multivar_dist;
results.B = B;
results.FitInfo = FitInfo;

end